$(window).scroll(function() {
  var nav = $("#navbar");
  var top = 500;
  if ($(window).scrollTop() >= top) {
    nav.addClass("fixed");
  } else {
    nav.removeClass("fixed");
  }
});
